﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Sfb.Server.DotNetFramework.Startup))]
namespace Sfb.Server.DotNetFramework
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
