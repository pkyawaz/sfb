﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNet.SignalR;
using Sfb.Common.DotNetFramework;

namespace Sfb.Server.DotNetFramework.Hubs
{
    public class SfbHub : Hub
    {
        public static List<SfbConversation> Conversations = new List<SfbConversation>
        {
            new SfbConversation
            {
                Name = "Test", Messages = new List<SfbMessage>
                {
                    new SfbMessage { Message  = "This is a test" , Sender = "Phillip"}
                }
            }
        };

        public void SendMessage(string to, string message)
        {
            Clients.Others.SendMessage(to, message);
        }

        public void UpdateConversations(List<SfbConversation> conversations)
        {
            Conversations = conversations;
            Clients.Others.updateClientConversations(conversations);
        }


        public void GetConversations()
        {
            Clients.Caller.updateClientConversations(Conversations);
        }
    }

    public interface ISfbConversationService

    {
        Task UpdateConversations(List<SfbConversation> conversations);
        Task GetConversations();
        Task SendMessage(string to, string message);
    }
}