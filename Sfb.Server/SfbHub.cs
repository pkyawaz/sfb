﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.SignalR;
using Sfb.Common;

namespace Sfb.Server
{
    public class SfbHub : Hub<ISfbConversationService>
    {
        public static List<SfbConversation> Conversations = new List<SfbConversation>();

        public async Task SendMessage(string to, string message)
        {
            await Clients.All.SendMessage(to, message);
        }

        public async Task UpdateConversations(List<SfbConversation> conversations)
        {
            Conversations = conversations;
            await Clients.Others.UpdateConversations(conversations);
        }


        public async Task GetConversations()
        {
             await Clients.Caller.UpdateConversations(Conversations);
        }
    }

    public interface ISfbConversationService

    {
        Task UpdateConversations(List<SfbConversation> conversations);
        Task GetConversations();
        Task SendMessage(string to, string message);
    }
}