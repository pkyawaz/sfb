﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace Sfb.Common
{
    public class SfbMessage
    {
        [JsonProperty("sender")]
        public string Sender { get; set; }
        [JsonProperty("message")]
        public string Message { get; set; }
    }

    public class SfbConversation
    {
        [JsonProperty("name")]
        public string Name { get; set; }
        [JsonProperty("messages")]
        public List<SfbMessage> Messages { get; set; }
    }
}