﻿using Microsoft.AspNetCore.SignalR.Client;
using Sfb.Common;
using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Threading;
using System.Threading.Tasks;

namespace Sfb.Client
{
    class Program
    {
        static void Main(string[] args)
        {
            Thread.Sleep(10000);
            var host = args.Length == 0 ? "alt36.azurewebsites.net" : args[0];

            var connection = new HubConnectionBuilder().WithUrl($"https://{host}/SfbHub").Build();
            connection.Closed += async (error) =>
           {
               await Task.Delay(new Random().Next(0, 5) * 1000);
               await connection.StartAsync();
           };
            connection.On<string,string>("SendMessage", (to, message) =>
                {
                    Console.WriteLine(to + ":" + message);
                }
            );

            connection.StartAsync().Wait();

            //Worker worker = new Worker();
            //Thread t = new Thread(worker.DoWork);
            //t.IsBackground = true;
            //t.Start();

            //while (true)
            //{
            //    var keyInfo = Console.ReadKey();
            //    if (keyInfo.Key == ConsoleKey.C)
            //    {
            //        worker.KeepGoing = false;
            //        break;
            //    }
            //}
            //t.Join();


            connection.InvokeAsync("UpdateConversations", new List<SfbConversation>
            {
                new SfbConversation
                {
                    Name = "Test1",
                    Messages = new List<SfbMessage>
                    {
                        new SfbMessage
                        {
                            Sender = "Phillip",
                            Message = "Message Test1"
                        }
                    }
                }
            }).Wait();


            Console.Read();
            connection.StopAsync().Wait();

            //Thread.Sleep(1000000000);
            //try
            //{
            //    //Start the conversation
            //    Automation automation = LyncClient.GetAutomation();
            //    LyncClient client = LyncClient.GetClient();

            //    var conversations = client.ConversationManager.Conversations.Select(JsonConvert.SerializeObject).ToList();


            //}
            //catch (LyncClientException lyncClientException)
            //{
            //    Console.Out.WriteLine("Failed to connect to Lync.");
            //    Console.Out.WriteLine(lyncClientException);
            //}
            //catch (SystemException systemException)
            //{
            //    if (IsLyncException(systemException))
            //    {
            //        // Log the exception thrown by the Lync Model API.
            //        Console.Out.WriteLine("Failed to connect to Lync.");
            //        Console.WriteLine("Error: " + systemException);
            //    }
            //    else
            //    {
            //        // Rethrow the SystemException which did not come from the Lync Model API.
            //        throw;
            //    }
            //}

            return;
        }

        private static bool IsLyncException(SystemException ex)
        {
            return
                ex is NotImplementedException ||
                ex is ArgumentException ||
                ex is NullReferenceException ||
                ex is NotSupportedException ||
                ex is ArgumentOutOfRangeException ||
                ex is IndexOutOfRangeException ||
                ex is InvalidOperationException ||
                ex is TypeLoadException ||
                ex is TypeInitializationException ||
                ex is InvalidComObjectException ||
                ex is InvalidCastException;
        }
    }

    class Worker
    {
        public bool KeepGoing { get; set; }

        public Worker()
        {
            KeepGoing = true;
        }

        public void DoWork()
        {
            while (KeepGoing)
            {
                Console.WriteLine("Ding");
                Thread.Sleep(200);
            }
        }
    }
}
