﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace Sfb.Common.DotNetFramework
{
    public class SfbMessage
    {
        [JsonProperty("sender")]
        public string Sender { get; set; }
        [JsonProperty("message")]
        public string Message { get; set; }
    }

    public class SfbConversation
    {
        [JsonProperty("name")]
        public string Name { get; set; }
        [JsonProperty("messages")]
        public List<SfbMessage> Messages { get; set; }
    }
}
